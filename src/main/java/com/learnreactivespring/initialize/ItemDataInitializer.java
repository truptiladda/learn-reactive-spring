package com.learnreactivespring.initialize;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.learnreactivespring.documents.Item;
import com.learnreactivespring.documents.ItemCapped;
import com.learnreactivespring.repository.ItemReactiveRepository;

import reactor.core.publisher.Flux;

@Component
public class ItemDataInitializer implements CommandLineRunner {

	@Autowired
	private ItemReactiveRepository itemReactiveRepository;

	@Autowired
	MongoTemplate mongoOperations;

	@Override
	public void run(String... args) throws Exception {
		initialDataSetup();

	}

	public void createCappedCollection() {

		mongoOperations.dropCollection(ItemCapped.class);
		mongoOperations.createCollection(ItemCapped.class,
				CollectionOptions.empty().maxDocuments(20).size(50000).capped());

	}

	private void initialDataSetup() {
		itemReactiveRepository.deleteAll().thenMany(Flux.fromIterable(data())).flatMap(itemReactiveRepository::save)
				.thenMany(itemReactiveRepository.findAll()).subscribe(item -> {
					System.out.println("Item inserted from CommanLineRunneris : " + item);
				});

	}

	public List<Item> data() {
		return Arrays.asList(new Item(null, " Samsung Tv", 520.0), new Item(null, " LG Tv", 329.0),
				new Item(null, " Aple watch", 349.0), new Item("ABC", "Boat HEADPHONES", 120.0));
	}

}
