package com.learnreactivespring.controller;

import static com.learnreactivespring.constants.ConfigConstants.ITEM_ENDPOINT_V1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.learnreactivespring.documents.Item;
import com.learnreactivespring.repository.ItemReactiveRepository;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@Slf4j
public class ItemController {

	@Autowired
	private ItemReactiveRepository itemRepository;

	@GetMapping(ITEM_ENDPOINT_V1)
	public Flux<Item> getAllItems() {
		return itemRepository.findAll();
	}

	@GetMapping(ITEM_ENDPOINT_V1 + "/{id}")
	public Mono<ResponseEntity<Item>> getOneItemById(@PathVariable String id) {
		return itemRepository.findById(id).map(item -> new ResponseEntity<>(item, HttpStatus.OK))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}

	@PostMapping(ITEM_ENDPOINT_V1)
	@ResponseStatus(HttpStatus.CREATED)
	public Mono<Item> createItem(@RequestBody Item item) {

		return itemRepository.save(item);

	}

	@DeleteMapping(ITEM_ENDPOINT_V1 + "/{id}")
	public Mono<Void> deleteItem(@PathVariable String id) {

		return itemRepository.deleteById(id);

	}

	@PutMapping(ITEM_ENDPOINT_V1 + "/{id}")
	public Mono<ResponseEntity<Item>> updateItem(@PathVariable String id, @RequestBody Item item) {
		return itemRepository.findById(id).flatMap(currItem -> {
			currItem.setPrice(item.getPrice());
			currItem.setDescription(item.getDescription());
			return itemRepository.save(currItem);
		}).map(updatedItem -> new ResponseEntity<>(updatedItem, HttpStatus.OK))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}
	
	@GetMapping(ITEM_ENDPOINT_V1+"/runTimeException")
	public Flux<Item> runTimeException(){
		return itemRepository.findAll()
				.concatWith(Mono.error(new RuntimeException("Runtime exception occured")));
	}
	
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<String> handleRuntimeException(RuntimeException e){
		log.error("Runtime exception: ",e);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
	}

}
