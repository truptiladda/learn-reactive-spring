package com.learnreactivespring.controller;

import java.time.Duration;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import reactor.core.publisher.Flux;

@RestController
public class FluxAndMonoController {

	@GetMapping("/flux")
	public Flux<Integer> fluxEndPoint() {
		return Flux.just(1, 2, 3, 4).delayElements(Duration.ofSeconds(1)).log();

	}

	@GetMapping(value = "/fluxStream", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)

	public Flux<Long> fluxEndPointStream() {
		return Flux.
				interval(Duration.ofSeconds(1)).log();

	}

}
