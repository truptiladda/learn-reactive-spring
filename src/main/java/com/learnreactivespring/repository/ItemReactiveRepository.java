package com.learnreactivespring.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.learnreactivespring.documents.Item;

public interface ItemReactiveRepository extends ReactiveMongoRepository<Item, String> {

}
