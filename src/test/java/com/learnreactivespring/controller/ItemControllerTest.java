package com.learnreactivespring.controller;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.learnreactivespring.constants.ConfigConstants;
import com.learnreactivespring.documents.Item;
import com.learnreactivespring.repository.ItemReactiveRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SpringBootTest
@RunWith(SpringRunner.class)
@DirtiesContext
@AutoConfigureWebTestClient
public class ItemControllerTest {

	@Autowired
	WebTestClient webTestClient;

	@Autowired
	ItemReactiveRepository itemReactiveRepository;

	public List<Item> data() {
		return Arrays.asList(new Item(null, " Samsung Tv", 520.0), new Item(null, " LG Tv", 329.0),
				new Item(null, " Aple watch", 349.0), new Item("ABC", "Boat HEADPHONES", 120.0));
	}

	@Before
	public void setUp() {

		itemReactiveRepository.deleteAll().thenMany(Flux.fromIterable(data())).flatMap(itemReactiveRepository::save)
				.doOnNext(item -> {
					System.out.println("Inserted item is :" + item);
				}).blockLast();

	}

	@SuppressWarnings("deprecation")
	@Test
	public void getAllItem() {

		webTestClient.get().uri(ConfigConstants.ITEM_ENDPOINT_V1).exchange().expectStatus().isOk().expectHeader()
				.contentType(MediaType.APPLICATION_JSON).expectBodyList(Item.class).hasSize(4);

	}

	@Test
	public void getOneItem() {

		webTestClient.get().uri(ConfigConstants.ITEM_ENDPOINT_V1.concat("/{id}"), "ABC").exchange().expectStatus()
				.isOk().expectBody().jsonPath("$.price", 120.0);

	}

	@Test
	public void getOneItem2() {

		webTestClient.get().uri(ConfigConstants.ITEM_ENDPOINT_V1.concat("/{id}"), "rt").exchange().expectStatus()
				.isNotFound();

	}
	
	@Test
	public void createItem() {
		Item item = new Item(null, "Iphone X",1000.00);
		webTestClient.post().uri(ConfigConstants.ITEM_ENDPOINT_V1)
		.contentType(MediaType.APPLICATION_JSON)
		.body(Mono.just(item), Item.class)
		.exchange()
		.expectStatus().isCreated()
		.expectBody()
		.jsonPath("$.id").isNotEmpty();
	}
	
	@Test
	public void deleteItem() {
		webTestClient.delete().uri(ConfigConstants.ITEM_ENDPOINT_V1.concat("/{id}"), "ABC")
		.accept(MediaType.APPLICATION_JSON)
		.exchange()
		.expectStatus().isOk()
		.expectBody(Void.class);
	}
	
	@Test
	public void updateItem() {
		double newPrice = 160.00;
		Item item = new Item(null, "Boat HEADPHONES",newPrice );
		webTestClient.put().uri(ConfigConstants.ITEM_ENDPOINT_V1.concat("/{id}"), "ABC")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.body(Mono.just(item), Item.class)
		.exchange()
		.expectStatus().isOk()
		.expectBody()
		.jsonPath("$.price", newPrice);
		
		
	}
	
	@Test
	public void updateItem_2() {
		double newPrice = 160.00;
		Item item = new Item(null, "Boat HEADPHONES",newPrice );
		webTestClient.put().uri(ConfigConstants.ITEM_ENDPOINT_V1.concat("/{id}"), "DEF")
		.contentType(MediaType.APPLICATION_JSON)
		.accept(MediaType.APPLICATION_JSON)
		.body(Mono.just(item), Item.class)
		.exchange()
		.expectStatus().isNotFound();
		
	}

}
