package com.learnreactivespring.repository;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.learnreactivespring.documents.Item;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

@DataMongoTest
@RunWith(SpringRunner.class)
public class ItemReactiveRepositoryTest {

	List<Item> items = Arrays.asList(new Item(null, "Samsung Tv", 500.0), new Item(null, "Apple watch", 200.0),
			new Item(null, "Boat headphones", 100.0));

	@Autowired
	ItemReactiveRepository itemReactiveRepository;
	
	@Before
	public void setUp() {
		itemReactiveRepository.deleteAll()
		.thenMany(Flux.fromIterable(items))
		.flatMap(itemReactiveRepository::save)
		.doOnNext(item -> {
			System.out.println("Intesrted Item is :"+item);
		})
		.blockLast();
	}

	@Test
	public void getAllItems() {
		StepVerifier.create(itemReactiveRepository.findAll()).expectSubscription().expectNextCount(4).verifyComplete();
	}

}
