package com.learnreactivespring.fluxAndMonoPlayground;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class FluxAndMonoBackPressureTest {

	@Test
	public void backPressureTest() {

		Flux<Integer> flux = Flux.range(1, 10).log();

		StepVerifier.create(flux).expectSubscription().thenRequest(1).expectNext(1).thenCancel().verify();

	}

	@Test
	public void backPressure() {

		Flux<Integer> flux = Flux.range(1, 10).log();

		flux.subscribe((element) -> System.out.println("Element is :" + element),
				(e) -> System.out.println("Exception occured"), () -> System.out.println("DOne"),
				(subscription -> subscription.request(2)));

	}

	@Test
	public void backPressure_cancel() {

		Flux<Integer> flux = Flux.range(1, 10).log();

		flux.subscribe((element) -> System.out.println("Element is :" + element),
				(e) -> System.out.println("Exception occured"), () -> System.out.println("DOne"),
				(subscription -> subscription.cancel()));

	}
	
	@Test
	public void backPressure_custamized() {

		Flux<Integer> flux = Flux.range(1, 10).log();

		flux.subscribe(new BaseSubscriber<Integer>() {
			
			@Override
			protected void hookOnNext(Integer value) {
				request(1);
				System.out.println("value is "+value);
				if(value==4) {
					cancel();
				}
			}
		});

	}

}
