package com.learnreactivespring.fluxAndMonoPlayground;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class FluxAndMonoFactoryTests {

	List<String> names = Arrays.asList("Trupti", "Prajakta", "Khushboo");

	@Test
	public void fluxWithIterator() {
		Flux<String> namesFlux = Flux.fromIterable(names).log();

		StepVerifier.create(namesFlux).expectNext("Trupti", "Prajakta", "Khushboo").verifyComplete();

	}

	@Test
	public void fluxWithArray() {

		String[] names = new String[] { "Trupti", "Prajakta", "Khushboo" };
		Flux<String> namesFlux = Flux.fromArray(names);

		StepVerifier.create(namesFlux).expectNext("Trupti", "Prajakta", "Khushboo").verifyComplete();

	}

	@Test
	public void fluxWithStreams() {

		Flux<String> namesFlux = Flux.fromStream(names.stream());

		StepVerifier.create(namesFlux).expectNext("Trupti", "Prajakta", "Khushboo").verifyComplete();

	}

	@Test
	public void monoWithJusOrMono() {

		Mono<String> mono = Mono.justOrEmpty(null);
		StepVerifier.create(mono.log()).verifyComplete();
	}

	@Test
	public void monoWithSupplier() {

		Supplier<String> supplier = () -> "Trupti";

		Mono<String> mono = Mono.fromSupplier(supplier);
		StepVerifier.create(mono.log()).expectNext("Trupti").verifyComplete();
	}

	@Test
	public void fluxWithRange() {
		Flux<Integer> range = Flux.range(1, 5).log();
		StepVerifier.create(range).expectNext(1, 2, 3, 4, 5).verifyComplete();

	}

}
