package com.learnreactivespring.fluxAndMonoPlayground;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class FluxAndMonoCombineTest {

	// merge does not keep sequence of elements but works in parellel concat works opposite to that
	@Test
	public void combineFluxUsingMerge() {

		Flux<String> flux1 = Flux.just("A", "B", "C");
		Flux<String> flux2 = Flux.just("D", "E", "F");
		Flux<String> mergedFlux = Flux.merge(flux1, flux2).log();

		StepVerifier.create(mergedFlux).expectNext("A", "B", "C", "D", "E", "F").verifyComplete();

	}

	@Test
	public void combineFluxUsingConcat() {

		Flux<String> flux1 = Flux.just("A", "B", "C").delayElements(Duration.ofSeconds(1));
		Flux<String> flux2 = Flux.just("D", "E", "F").delayElements(Duration.ofSeconds(1));
		Flux<String> mergedFlux = Flux.concat(flux1, flux2).log();

		StepVerifier.create(mergedFlux).expectNext("A", "B", "C", "D", "E", "F").verifyComplete();

	}

}
