package com.learnreactivespring.fluxAndMonoPlayground;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

public class ColdAndHotpublisherTest {

	@Test
	public void coldPublisherTest() throws InterruptedException {
		Flux<String> flux = Flux.just("A", "B", "C").delayElements(Duration.ofSeconds(1)).log();

		flux.subscribe(s -> System.out.println("subscriber 1: " + s));
		Thread.sleep(2000);
		flux.subscribe(s -> System.out.println("subscriber 2: " + s));
		Thread.sleep(4000);

	}

	@Test
	public void hotPublisher() throws InterruptedException {

		Flux<String> flux = Flux.just("A", "B", "C").delayElements(Duration.ofSeconds(1));
		ConnectableFlux<String> connectableFlux = flux.publish();
		connectableFlux.connect();
		connectableFlux.subscribe(s -> System.out.println("subscriber 1: " + s));
		Thread.sleep(2000);
		connectableFlux.subscribe(s -> System.out.println("subscriber 2: " + s));
		Thread.sleep(4000);

	}

}
