package com.learnreactivespring.fluxAndMonoPlayground;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class FluxAndMonoFilterTests {

	List<String> names = Arrays.asList("Trupti", "Prajakta", "Khushboo", "Pooja");

	@Test
	public void filterTest() {
		Flux<String> stringFlux = Flux.fromIterable(names).filter(s -> s.startsWith("P")).log();
		StepVerifier.create(stringFlux).expectNext("Prajakta", "Pooja").verifyComplete();
	}

}
