package com.learnreactivespring.fluxAndMonoPlayground;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class FluxAndMonoTransformTest {

	List<String> names = Arrays.asList("Trupti", "Prajakta", "Khushboo", "Pooja");

	@Test
	public void transformUsingMap() {
		Flux<String> namesFlux = Flux.fromIterable(names).map(s -> s.toUpperCase());

		StepVerifier.create(namesFlux).expectNext("TRUPTI", "PRAJAKTA", "KHUSHBOO", "POOJA").verifyComplete();

	}

	@Test
	public void transformUsingMap_Filter() {
		Flux<String> namesFlux = Flux.fromIterable(names).filter(s -> s.length() > 5).map(s -> s.toUpperCase()).log();

		StepVerifier.create(namesFlux).expectNext("TRUPTI", "PRAJAKTA", "KHUSHBOO").verifyComplete();

	}

	@Test
	public void transformUsingFlatMap() {
		Flux<String> stringFlux = Flux.fromIterable(Arrays.asList("A", "B", "C", "D")).flatMap(s -> {
			return Flux.fromIterable(convertToList(s));
		}).log();
		StepVerifier.create(stringFlux).expectNextCount(8).verifyComplete();
	}

	private List<String> convertToList(String s) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Arrays.asList(s, "newVlaue");
	}
	 
	/*
	 * @Test public void transformUsingFilterMap_UsingParellel() { Flux<String>
	 * stringFlux = Flux.fromIterable(Arrays.asList("A", "B", "C", "D")) .window(2)
	 * .flatMap((s) -> { s.map(this::convertToList).subscribeOn(parallel())
	 * }).log();
	 * StepVerifier.create(stringFlux).expectNextCount(8).verifyComplete(); }
	 */
}
