package com.learnreactivespring.fluxAndMonoPlayground;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class FluxAndMonoTest {

	/*
	 * @Test public void fluxTest() {
	 * 
	 * Flux<String> stringFlux = Flux.just("Spring", "Spring boot",
	 * "Reactive spring") // .concatWith(Flux.error(new
	 * RuntimeException("Exception Occured")))
	 * .concatWith(Flux.just("After Error")).log();
	 * 
	 * stringFlux.subscribe(System.out::println, e -> System.err.println(e), () ->
	 * System.out.println("Completed"));
	 * 
	 * }
	 */

	@Test
	public void fluxTestElementsWithError() {

		Flux<String> stringFlux = Flux.just("Spring", "Spring boot", "Reactive spring")
				.concatWith(Flux.error(new RuntimeException("Exception Occured"))).log();

		StepVerifier.create(stringFlux).expectNext("Spring").expectNext("Spring boot").expectNext("Reactive spring")
				.expectError(RuntimeException.class).verify();

	}

	@Test
	public void monoTest() {
		Mono<String> stringMono = Mono.just("Spring");
		StepVerifier.create(stringMono.log()).expectNext("Spring").verifyComplete();

	}

	@Test
	public void monoTest_withError() {

		StepVerifier.create(Mono.error(new RuntimeException("Exception occured")).log()).expectError().verify();

	}

}
