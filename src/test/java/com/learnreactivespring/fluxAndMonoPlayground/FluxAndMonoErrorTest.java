package com.learnreactivespring.fluxAndMonoPlayground;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class FluxAndMonoErrorTest {

	@Test
	public void fluxErrorHandling() {

		Flux<String> stringFlux = Flux.just("A", "B", "C")
				.concatWith(Flux.error(new RuntimeException("Ac=xception occured"))).concatWith(Flux.just("D"))
				.onErrorResume((e) -> {
					System.out.println("Exception is : " + e);
					return Flux.just("default");
				}).log();
		StepVerifier.create(stringFlux).expectSubscription().expectNext("A", "B", "C").expectNext("default")
				.verifyComplete();

	}
	
	@Test
	public void fluxErrorHandlingOnErrorReturn() {

		Flux<String> stringFlux = Flux.just("A", "B", "C")
				.concatWith(Flux.error(new RuntimeException("Ac=xception occured"))).concatWith(Flux.just("D"))
				.onErrorReturn("default").log();
		StepVerifier.create(stringFlux).expectSubscription().expectNext("A", "B", "C").expectNext("default")
				.verifyComplete();

	}

}
